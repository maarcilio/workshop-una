namespace WorkShopUNA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriacaoDasTabelas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agenda",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        Sexo = c.Int(nullable: false),
                        Idade = c.DateTime(nullable: false),
                        AgendaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agenda", t => t.AgendaId, cascadeDelete: true)
                .Index(t => t.AgendaId);
            
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CEP = c.String(nullable: false),
                        Rua = c.String(nullable: false),
                        Bairro = c.String(nullable: false),
                        Cidade = c.String(nullable: false),
                        Estado = c.String(nullable: false),
                        Complemento = c.String(),
                        ContatoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contato", t => t.ContatoId, cascadeDelete: true)
                .Index(t => t.ContatoId);
            
            CreateTable(
                "dbo.Telefone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DDD = c.String(nullable: false),
                        Numero = c.String(nullable: false),
                        Ramal = c.String(),
                        TipoTelefone = c.Int(nullable: false),
                        ContatoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contato", t => t.ContatoId, cascadeDelete: true)
                .Index(t => t.ContatoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Telefone", "ContatoId", "dbo.Contato");
            DropForeignKey("dbo.Endereco", "ContatoId", "dbo.Contato");
            DropForeignKey("dbo.Contato", "AgendaId", "dbo.Agenda");
            DropIndex("dbo.Telefone", new[] { "ContatoId" });
            DropIndex("dbo.Endereco", new[] { "ContatoId" });
            DropIndex("dbo.Contato", new[] { "AgendaId" });
            DropTable("dbo.Telefone");
            DropTable("dbo.Endereco");
            DropTable("dbo.Contato");
            DropTable("dbo.Agenda");
        }
    }
}
