namespace WorkShopUNA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AltrandoColunaIdadeParaDataNascimentTabelaContato : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contato", "DataNascimento", c => c.DateTime(nullable: false));
            DropColumn("dbo.Contato", "Idade");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contato", "Idade", c => c.DateTime(nullable: false));
            DropColumn("dbo.Contato", "DataNascimento");
        }
    }
}
