namespace WorkShopUNA.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
	using WorkShopUNA.Models.Contexto;

    internal sealed class Configuration : DbMigrationsConfiguration<AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WorkShopUNA.Models.Contexto.AppContext context)
        {
            
        }
    }
}
