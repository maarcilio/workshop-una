﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WorkShopUNA.Models.Enum;

namespace WorkShopUNA.Models
{
	[Table("Telefone")]
	public class Telefone
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual int Id { get; set; }

		[Required]
		public virtual string DDD { get; set; }

		[Required]
		public virtual string Numero { get; set; }

		public virtual string Ramal { get; set; }

		[Required]
		public virtual TipoTelefone TipoTelefone { get; set; }

		public int ContatoId { get; set; }

		[ForeignKey("ContatoId")]
		public virtual Contato Contato { get; set; }
	}
}