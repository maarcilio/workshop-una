﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WorkShopUNA.Models.Contexto
{
	public class AppContext : DbContext
	{
		public AppContext()
			: base("BancoDeDados")
		{
		}

		public DbSet<Agenda> Agenda { get; set; }

		public DbSet<Contato> Contato { get; set; }

		public DbSet<Telefone> Telefone { get; set; }

		public DbSet<Endereco> Endereco { get; set; }

	}
}