﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkShopUNA.Models.Enum
{
	public enum TipoTelefone
	{
		Fixo,
		Celular,
		Trabalho
	}
}