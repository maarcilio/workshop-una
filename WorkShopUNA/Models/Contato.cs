﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WorkShopUNA.Models.Enum;

namespace WorkShopUNA.Models
{
	[Table("Contato")]
	public class Contato
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual int Id { get; set; }

		[Required]
		public virtual string Nome { get; set; }

		[Required]
		public virtual Sexo Sexo { get; set; }

		[Required]
		[Display(Name = "Data de Nascimento")]
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		[DataType(DataType.DateTime)]
		public virtual DateTime DataNascimento { get; set; }

		[NotMapped]
		public int Idade
		{
			get
			{
				if(DataNascimento == DateTime.MinValue)
					return 0;
				else
				{
					DateTime hoje = DateTime.Today;
					int idade = hoje.Year - DataNascimento.Year;
					if (DataNascimento > hoje.AddYears(-idade)) idade--;
					return idade;
				}
			}
		}

		public int AgendaId { get; set; }

		[ForeignKey("AgendaId")]
		public virtual Agenda Agenda { get; set; }

		public virtual IList<Endereco> Enderecos { get; set; }

		public virtual IList<Telefone> Telefones { get; set; }

		public Contato()
		{
			Enderecos = new List<Endereco>();
			Telefones = new List<Telefone>();
		}
	}
}