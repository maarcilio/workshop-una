﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkShopUNA.Models
{
	[Table("Agenda")]
	public class Agenda
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual int Id { get; set; }

		[StringLength(255)]
		[Required]
		public virtual string Nome { get; set; }

		public virtual IList<Contato> Contatos { get; set; }

		public Agenda()
		{
			Contatos = new List<Contato>();
		}
	}
}