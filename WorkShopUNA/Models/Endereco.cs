﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkShopUNA.Models
{
	[Table("Endereco")]
	public class Endereco
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual int Id { get; set; }

		[Required]
		public virtual string CEP { get; set; }

		[Required]
		public virtual string Rua { get; set; }

		[Required]
		public virtual string Bairro { get; set; }

		[Required]
		public virtual string Cidade { get; set; }

		[Required]
		public virtual string Estado { get; set; }

		public virtual string Complemento { get; set; }

		public int ContatoId { get; set; }

		[ForeignKey("ContatoId")]
		public virtual Contato Contato { get; set; }
	}
}