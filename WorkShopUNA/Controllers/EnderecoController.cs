﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkShopUNA.Models;
using WorkShopUNA.Models.Contexto;

namespace WorkShopUNA.Controllers
{
    public class EnderecoController : Controller
    {
        private AppContext db = new AppContext();

        // GET: Endereco
		public ActionResult Index(int Id)
        {
			ViewBag.ContatoId = Id;
            var endereco = db.Endereco.Include(e => e.Contato).Where(x=>x.Contato.Id == Id);
            return View(endereco.ToList());
        }

        // GET: Endereco/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endereco endereco = db.Endereco.Find(id);
            if (endereco == null)
            {
                return HttpNotFound();
            }
            return View(endereco);
        }

        // GET: Endereco/Create
		public ActionResult Create(int ContatoId)
        {
			ViewBag.ContatoId = ContatoId;
            return View();
        }

        // POST: Endereco/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CEP,Rua,Bairro,Cidade,Estado,Complemento,ContatoId")] Endereco endereco)
        {
            if (ModelState.IsValid)
            {
                db.Endereco.Add(endereco);
                db.SaveChanges();
				return RedirectToAction("Index", new { Id = endereco.ContatoId });
            }

			ViewBag.ContatoId = endereco.ContatoId;
            return View(endereco);
        }

        // GET: Endereco/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endereco endereco = db.Endereco.Find(id);
            if (endereco == null)
            {
                return HttpNotFound();
            }
			ViewBag.ContatoId = endereco.ContatoId;
            return View(endereco);
        }

        // POST: Endereco/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CEP,Rua,Bairro,Cidade,Estado,Complemento,ContatoId")] Endereco endereco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(endereco).State = EntityState.Modified;
                db.SaveChanges();
				return RedirectToAction("Index", new { Id = endereco.ContatoId });
            }
			ViewBag.ContatoId = endereco.ContatoId;
            return View(endereco);
        }

        // GET: Endereco/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endereco endereco = db.Endereco.Find(id);
            if (endereco == null)
            {
                return HttpNotFound();
            }
            return View(endereco);
        }

        // POST: Endereco/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Endereco endereco = db.Endereco.Find(id);
			int ContatoId = endereco.ContatoId;
            db.Endereco.Remove(endereco);
            db.SaveChanges();
			return RedirectToAction("Index", new { Id = ContatoId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
